

<div class="main">
    <div class="content" style="text-align: center">
        <div class="register_account" style="text-align:center;display:inline-block;float: none">
            <h3>Register New Account</h3>
            <style type="text/css">
                #result{color:red;padding: 5px}
                #result p{color:red}
            </style>
            <div id="result">
                <p><?php echo $this->session->flashdata('message'); ?></p>
            </div>
            <form method="post" action="<?php echo base_url('customer/save');?>">
                <table>
                    <tbody>
                        <tr>
                            <td>
                                <div>
                                    <input type="text" name="customer_name" placeholder="Masukan Nama">
                                </div>

                                <div>
                                    <input type="password" name="customer_password" placeholder="Masukan Password">

                                </div>

                                <div>
                                    <input type="text" name="customer_city" placeholder="Masukan Kota">
                                </div>
                                <div>
                                    <input type="text" name="customer_phone" placeholder="Masukan nomor hp">
                                </div>
                            </td>
                            <td>
                                <div>
                                    <input type="text" name="customer_email" placeholder="Masukan Email">
                                </div>
                                        

                                <div>
                                    <input type="text" name="customer_address" placeholder="Masukan Alamat">
                                </div>
                                
                                <div>
                                <select id="country" name="customer_country" class="frm-field required">
                                <option value="null">Pilih Provisi</option>         
                                        <option value="ST">Sulawesi Tengah</option>
                                        <option value="SN">Sulawesi Selatan</option>
                                        <option value="SR">Sulawesi Barat</option>
                                        <option value="SA">Sulawesi Utara</option>
                                        <option value="SG">Sulawesi Tenggara</option>
                                        <option value="GO">Gorontalo</option>
                                        <option value="JT">Jawa Tengah</option>
                                        <option value="JI">Jawa Timur</option>
                                        <option value="JB">Jawa Barat</option>
                                        <option value="YK">Yogyakarta</option>

                                    </select>
                                </div>		

                                <div>
                                    <input type="text" name="customer_zipcode" placeholder="Masukan Kode Pos">
                                </div>
                            </td>
                        </tr> 
                    </tbody></table> 
                <div class="search"><div><button class="grey">Buat Akun</button></div></div>
                <p class="terms">By clicking 'Buat Akun' you agree to the <a href="#">Terms &amp; Conditions</a>.</p>
                <div class="clear"></div>
            </form>
        </div>  	
        <div class="clear"></div>
    </div>
</div>